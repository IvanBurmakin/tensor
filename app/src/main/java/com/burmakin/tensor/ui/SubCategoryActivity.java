package com.burmakin.tensor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.burmakin.tensor.MyAdapter;
import com.burmakin.tensor.R;
import com.burmakin.tensor.model.Category;
import com.burmakin.tensor.model.ParentCategory;
import com.burmakin.tensor.model.SubCategory;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubCategoryActivity extends AppCompatActivity {

    private ParentCategory category;
    private MyAdapter myAdapter;
    private List<ParentCategory> list;

    @BindView(R.id.rv_sub_category)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);

        init();
        main();

    }

    private void init() {

        ButterKnife.bind(this);
    }

    private void main() {

        category = getIntent().getParcelableExtra(MainActivity.CURRENT_ITEM);
        if (category != null) {

            list = new ArrayList<>();
            list.addAll(((Category) category).getSubs());
            initRecView(list);
        }
    }

    private void initRecView(List<ParentCategory> list) {

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new MyAdapter(list);
        mRecyclerView.setAdapter(myAdapter);
        myAdapter.setOnItemClickListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ParentCategory category) {

                if (check(category)) {
                    startLowCategory(category);
                } else {
                    showSnackbar();
                }
            }
        });
    }

    private boolean check(ParentCategory category) {

        if ((((SubCategory) category).getSubs()).isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    private void showSnackbar() {

        Snackbar.make(mRecyclerView, getString(R.string.is_empty), Snackbar.LENGTH_LONG)
                .show();
    }

    private void startLowCategory(ParentCategory category) {

        Intent intent = new Intent(SubCategoryActivity.this, LowCategoryActivity.class);
        intent.putExtra(MainActivity.CURRENT_ITEM, category);
        startActivity(intent);

    }
}
