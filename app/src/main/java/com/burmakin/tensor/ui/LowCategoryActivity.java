package com.burmakin.tensor.ui;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.burmakin.tensor.MyAdapter;
import com.burmakin.tensor.R;
import com.burmakin.tensor.model.ParentCategory;
import com.burmakin.tensor.model.SubCategory;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LowCategoryActivity extends AppCompatActivity {

    private ParentCategory category;
    private List<ParentCategory> list;
    private MyAdapter myAdapter;

    @BindView(R.id.rv_low_category)
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_low_category);

        init();
        main();


    }

    private void init() {

        ButterKnife.bind(this);
    }

    private void main() {

        category = getIntent().getParcelableExtra(MainActivity.CURRENT_ITEM);
        if (category != null) {

            list = new ArrayList<>();
            list.addAll(((SubCategory) category).getSubs());
            initRecView(list);
        }
    }

    private void initRecView(List<ParentCategory> list) {

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new MyAdapter(list);
        mRecyclerView.setAdapter(myAdapter);
        myAdapter.setOnItemClickListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ParentCategory category) {

                showSnackbar();
            }
        });

    }

    private void showSnackbar() {

        Snackbar.make(mRecyclerView, getString(R.string.is_empty), Snackbar.LENGTH_LONG)
                .show();
    }
}

