package com.burmakin.tensor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.burmakin.tensor.MyAdapter;
import com.burmakin.tensor.R;
import com.burmakin.tensor.api.MyRetrofit;
import com.burmakin.tensor.api.RestAPI;
import com.burmakin.tensor.database.Database;
import com.burmakin.tensor.model.Category;
import com.burmakin.tensor.model.ParentCategory;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private List<ParentCategory> parentList;
    private Realm realm;
    private MyAdapter myAdapter;
    private Database database;
    private List<Category> list;
    private static RestAPI restAPI;
    public static final String CURRENT_ITEM = "CurrentItem";
    private static final String CONTENT_TYPE = "application/json";
    private static final String USER_KEY = "cbd2482f-5e46-4483-b9f9-ad667183a86b";
    private static final String PROJECT_KEY = "334e7727-f014-493e-87c2-3db789aa802a";
    private static final String ON_RESPONSE = "ON_RESPONSE";
    private static final String ON_FAILURE = "ON_FAILURE";

    @BindView(R.id.rv_main_activity)
    RecyclerView mRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        if ((database.getDataFromDataBase(realm).isEmpty())) { downloadDataFromAPI();}
        else { getDataFromBase();}

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        downloadDataFromAPI();
        return true;

    }

    private void init() {

        database = new Database();
        realm = Realm.getDefaultInstance();
        ButterKnife.bind(this);

    }


    private void getDataFromBase() {

        parentList = new ArrayList<>();
        parentList.addAll(database.getDataFromDataBase(realm));
        initRecView(parentList);
    }

    public void downloadDataFromAPI() {

        restAPI = MyRetrofit.getApi();
        Call<List<Category>> call = restAPI.downloadData(CONTENT_TYPE, USER_KEY, PROJECT_KEY);
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {

                if (response.isSuccessful()) {
                    list = new ArrayList<>();
                    list.addAll(response.body());
                    parentList = new ArrayList<>();
                    parentList.addAll(list);
                    initRecView(parentList);
                    database.fillDataBase(realm, list);

                } else {
                    Log.d(ON_RESPONSE, response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.d(ON_FAILURE, t.getMessage());
            }
        });
    }


    private void initRecView(List<ParentCategory> list) {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new MyAdapter(list);
        mRecyclerView.setAdapter(myAdapter);
        myAdapter.setOnItemClickListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ParentCategory category) {

                showSubCategory(category);
            }
        });

    }


    private void showSubCategory(ParentCategory category) {

        Intent intent = new Intent(MainActivity.this, SubCategoryActivity.class);
        intent.putExtra(CURRENT_ITEM, category);
        startActivity(intent);

    }

}
