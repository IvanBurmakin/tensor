package com.burmakin.tensor.database;

import com.burmakin.tensor.model.Category;

import java.util.List;

import io.realm.Realm;

public class Database {


    public void fillDataBase(Realm realm, final List<Category> list) {


        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                realm.deleteAll();
                realm.copyToRealm(list);

            }
        });

    }

    public List<Category> getDataFromDataBase(Realm realm) {

        return realm.copyFromRealm(realm.where(Category.class).findAll());

    }
}
