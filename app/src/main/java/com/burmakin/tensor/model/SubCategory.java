package com.burmakin.tensor.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;


public class SubCategory extends RealmObject implements ParentCategory {

    public SubCategory() {
    }

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("img")
    @Expose
    private String img;

    @SerializedName("subs")
    @Expose
    private RealmList<SubCategory> subs;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public RealmList<SubCategory> getSubs() {
        return subs;
    }

    public void setSubs(ArrayList<SubCategory> subs) {

        this.subs = new RealmList<>();
        this.subs.addAll(subs);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(img);
        dest.writeTypedList(subs);

    }

    public static final Parcelable.Creator<SubCategory> CREATOR = new Parcelable.Creator<SubCategory>() {

        @Override
        public SubCategory createFromParcel(Parcel source) {
            return new SubCategory(source);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };

    public SubCategory(Parcel parcel) {

        id = parcel.readString();
        title = parcel.readString();
        img = parcel.readString();
        subs = new RealmList<>();
        parcel.readTypedList(subs, SubCategory.CREATOR);

    }

}
