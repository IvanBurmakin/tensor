package com.burmakin.tensor.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;


public class Category extends RealmObject implements ParentCategory {

    public Category() {
    }

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("subs")
    @Expose
    private RealmList<SubCategory> subs;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public RealmList<SubCategory> getSubs() {
        return subs;
    }

    public void setSubs(ArrayList<SubCategory> subs) {
        this.subs = new RealmList<>();
        this.subs.addAll(subs);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(img);
        dest.writeTypedList(subs);

    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {

        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    private Category(Parcel parcel) {
        title = parcel.readString();
        img = parcel.readString();
        subs = new RealmList<>();
        parcel.readTypedList(subs, SubCategory.CREATOR);

    }
}
