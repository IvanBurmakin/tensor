package com.burmakin.tensor;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.burmakin.tensor.model.Category;
import com.burmakin.tensor.model.ParentCategory;
import com.burmakin.tensor.model.SubCategory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(ParentCategory category);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    private List<ParentCategory> parentList;

    public MyAdapter(List<ParentCategory> list) {
        this.parentList = list;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent,
                false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ParentCategory model = parentList.get(position);
        holder.bind(model);
    }

    @Override
    public int getItemCount() {
        return parentList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cat_title)
        TextView title;
        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.chevron_right)
        ImageView chevronRight;


        ParentCategory model;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bind(ParentCategory model) {
            this.model = model;
            if (model instanceof Category) {

                title.setText(((Category) model).getTitle());
                if(!((Category) model).getSubs().isEmpty()){
                    chevronRight.setImageResource(R.drawable.ic_chevron_right);
                }

            }

            if (model instanceof SubCategory) {
                title.setText(((SubCategory) model).getTitle());
                if(!((SubCategory) model).getSubs().isEmpty()){
                    chevronRight.setImageResource(R.drawable.ic_chevron_right);
                }
            }

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        ParentCategory category = parentList.get(getLayoutPosition());
                        listener.onItemClick(category);
                    }
                }
            });

        }


    }
}



