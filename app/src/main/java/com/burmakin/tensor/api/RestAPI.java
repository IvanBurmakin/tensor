package com.burmakin.tensor.api;


import com.burmakin.tensor.model.Category;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface RestAPI {



    @GET("test_api")
    Call<List<Category>> downloadData(@Header("Content-Type") String contentType,
                                     @Header("JsonStub-User-Key") String userKey,
                                     @Header("JsonStub-Project-Key") String projectKey );


}
